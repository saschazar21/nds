/*
 * Copyright 2016 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef CONSUMER_H
#define CONSUMER_H

class Consumer
{
public:
Consumer(CircularBuffer* buf);
~Consumer();
void* run();
int startThread();
int joinThread();
private:
  static void* _run(void* args);
  
  CircularBuffer* output;
  pthread_t pthr;
  void** retVal;
};

#endif // CONSUMER_H
