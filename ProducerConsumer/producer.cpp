/*
 * Copyright 2016 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include "circularbuffer.h"
#include "producer.h"

Producer::Producer(CircularBuffer* buf) : output(buf)
{
  
}

Producer::~Producer()
{
 
}

int Producer::startThread()
{
  int ret = 0;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  int threadExit = pthread_create(&pthr, &attr, Producer::_run, this);
  if (threadExit != 0) {
  
    ret = -1;
  }
  pthread_attr_destroy(&attr);
  return ret;
}

int Producer::joinThread()
{
  return pthread_join(pthr, retVal);
}

void* Producer::run()
{
  try {
    
    while(true) {
    
      int datum = std::rand() % 100000;
      this->output->put(datum);
      std::cout << "Put " <<  datum << std::endl;
      
      int sleep = std::rand() % 2000000;
      
      usleep(sleep);
    }
  } catch(int err) {
  
    std::cout << "Producer exited..." << std::endl;
  }
}

void* Producer::_run(void* args)
{
  return ((Producer*)args)->run();
}

