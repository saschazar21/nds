#include <iostream>
#include "circularbuffer.h"
#include "producer.h"
#include "consumer.h"

int main(int argc, char **argv) {
    
  CircularBuffer* cbuf = new CircularBuffer();
  
  Producer* prod = new Producer(cbuf);
  Consumer* cons = new Consumer(cbuf);
  
  if (prod->startThread() < 0) {
  
    std::cout << "Something went wrong while creating the producer thread." << std::endl;
    return -1;
  }if (cons->startThread() < 0) {
  
    std::cout << "Something went wrong while creating the consumer thread." << std::endl;
    return -1;
  }
  prod->joinThread();
  cons->joinThread();
  
  delete cons;
  delete prod;
  delete cbuf;
  return 0;
}
