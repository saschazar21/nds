/*
 * Copyright 2016 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

class CircularBuffer
{
public:
  CircularBuffer();
  ~CircularBuffer();
  int getValueFromBuffer();
  int putValueIntoBuffer(int value);
  void put(int value);
  int get();
private:
  bool isEmpty();
  bool isFull();
  
  static const int MAX_SIZE;
  int base;
  int size;
  int* buffer;
  
  pthread_mutex_t mutex;
  pthread_mutexattr_t attribute;
  pthread_cond_t condition;
  pthread_condattr_t cond_attr;
};

#endif // CIRCULARBUFFER_H
