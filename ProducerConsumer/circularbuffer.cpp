/*
 * Copyright 2016 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include <pthread.h>
#include "circularbuffer.h"

const int CircularBuffer::MAX_SIZE = 8;

CircularBuffer::CircularBuffer()
{
  this->base = 0;
  this->size = 0;
  this->buffer = new int[CircularBuffer::MAX_SIZE];
  
  pthread_mutexattr_init(&attribute);
  pthread_mutexattr_settype(&attribute, PTHREAD_MUTEX_RECURSIVE_NP);
  pthread_mutex_init(&mutex, &attribute);
  pthread_mutexattr_destroy(&attribute);
  pthread_cond_init(&condition, 0);
}

CircularBuffer::~CircularBuffer()
{
  delete this->buffer;
}

int CircularBuffer::getValueFromBuffer()
{
  int index = this->base;
  this->base = (this->base + 1) % MAX_SIZE;
  this->size--;						/*@ \label{changesize} @*/
  return this->buffer[index];
}

int CircularBuffer::putValueIntoBuffer(int value)
{
  int index = (this->base + this->size) % MAX_SIZE;
  this->buffer[index] = value;	/*@ \label{schedule} @*/
  this->size++;
}

void CircularBuffer::put(int value)
{
  pthread_mutex_lock(&mutex);
  
  while(isFull()) {
    pthread_cond_wait(&condition, &mutex);
  }
  
  pthread_mutex_unlock(&mutex);
  
  putValueIntoBuffer(value);
  
  pthread_mutex_lock(&mutex);
  pthread_cond_signal(&condition);
  pthread_mutex_unlock(&mutex);
}

int CircularBuffer::get()
{
  pthread_mutex_lock(&mutex);
 
  while(isEmpty()) {
   pthread_cond_wait(&condition, &mutex);
  }
  pthread_mutex_unlock(&mutex);
 
  int retValue = getValueFromBuffer();
 
  pthread_mutex_lock(&mutex);
  pthread_cond_signal(&condition);
  pthread_mutex_unlock(&mutex);
 
  return retValue;
}


bool CircularBuffer::isEmpty()
{
  return this->size == 0;
}

bool CircularBuffer::isFull()
{
  return this->size == this->MAX_SIZE;
}

