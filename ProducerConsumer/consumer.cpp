/*
 * Copyright 2016 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include "circularbuffer.h"
#include "consumer.h"

Consumer::Consumer(CircularBuffer* buf) : output(buf)
{
  
}

Consumer::~Consumer()
{
  
}

int Consumer::startThread()
{
  int ret = 0;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  if (pthread_create(&pthr, &attr, Consumer::_run, this) != 0) {
    ret = -1;
  }
  pthread_attr_destroy(&attr);
  return ret;
}

int Consumer::joinThread()
{
  return pthread_join(pthr, retVal);
}

void* Consumer::run()
{
  try {
    
    while(true) {
      
      int datum = this->output->get();
      std::cout << "	Got: " << datum << std::endl;
      usleep(1000000);
    } 
  } catch(int err) {
    std::cout << "	Consumer exited..." << std::endl;
  }
}

void* Consumer::_run(void* args)
{
  return ((Consumer*)args)->run();
}


